# Profit

It's a Unity package with a set of useful tools (Runtime + Editor) that can to speed-up development. It consists of many extensions and helpers of different kinds, which can be used across the scripting.

# Structure

* ## Editor
* ## Runtime

    * Extensions
        * Collection
	    * Component
		* Coroutine
		* GameObject
		* Math
		* Random
		* Transform
		* Vector