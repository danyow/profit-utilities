﻿using System;
using Opacha.Profit.TextDecoring;
using UnityEngine;

/* TODO:
 - think about call print code inline in each method to avoid extra method-call overhead
 - move settings constants to separate file
 */

public static class Logger
{
    private const string GAME_TAG = "GAME";
    private const string COLOR_TAG = "22e";
    private const string L_SEP = "[";
    private const string R_SEP = "]";
    
    private static readonly string logTag =
        $"{$"{L_SEP}{GAME_TAG}{R_SEP}".Style(StyleType.Bold).Color(COLOR_TAG)} {{0}} {L_SEP}{{1}}{R_SEP}";
    
    static string decorString = String.Empty;
    
    public static void Log(this object source, string title = null) => Debug.LogFormat(logTag, title, source);

    public static void Log(this object source, string title, params IDecorer[] decorers)
    {
        decorString = source.ToString();
        foreach (var decorer in decorers)
        {
            decorString = decorer.Decor(decorString);
        }
        decorString.Log(title);
    }
    
    public static void Log(this object source, StyleType style, string colorString, string title = null) => source.ToString().Style(style).Color(colorString).Log(title);

    public static void LogColor(this object source, string color, string title = null) => source.ToString().Color(color).Log(title);
    public static void LogStyle(this object source, StyleType style, string title = null) => source.ToString().Style(style).Log(title);
    
    #region Warning (similar to Log)

    public static void Warn(this object source, string title = null) => Debug.LogWarningFormat(logTag, title, source);

    public static void Warn(this object source, string title, params IDecorer[] decorers)
    {
        decorString = source.ToString();
        foreach (var decorer in decorers)
        {
            decorString = decorer.Decor(decorString);
        }
        decorString.Warn(title);
    }
    
    public static void Warn(this object source, StyleType style, string colorString, string title = null) => source.ToString().Style(style).Color(colorString).Warn(title);

    public static void WarnColor(this object source, string color, string title = null) => source.ToString().Color(color).Warn(title);
    public static void WarnStyle(this object source, StyleType style, string title = null) => source.ToString().Style(style).Warn(title);

    #endregion
    
    #region Error (similar to Log)

    public static void Error(this object source, string title = null) => Debug.LogErrorFormat(logTag, title, source);

    public static void Error(this object source, string title, params IDecorer[] decorers)
    {
        decorString = source.ToString();
        foreach (var decorer in decorers)
        {
            decorString = decorer.Decor(decorString);
        }
        decorString.Error(title);
    }
    
    public static void Error(this object source, StyleType style, string colorString, string title = null) => source.ToString().Style(style).Color(colorString).Error(title);

    public static void ErrorColor(this object source, string color, string title = null) => source.ToString().Color(color).Error(title);
    public static void ErrorStyle(this object source, StyleType style, string title = null) => source.ToString().Style(style).Error(title);

    #endregion

    public static void Assert(this bool condition, string message = "") => Debug.Assert(condition, message);
}

