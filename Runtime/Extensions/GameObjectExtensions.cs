using UnityEngine;

namespace Opacha.Profit.Extensions.GameObjects
{
    public static class GameObjectExtensions
    {
        #region Components

        public static bool TryGetComponentInChildren<T>(this GameObject gameObject, out T component, bool includeInactive = false)
        {
            component = gameObject.GetComponentInChildren<T>(includeInactive);
            return component != null;
        }
        
        public static bool TryGetComponentInParent<T>(this GameObject gameObject, out T component)
        {
            component = gameObject.GetComponentInParent<T>();
            return component != null;
        }

        public static bool HasComponent<T>(this GameObject gameObject)
        {
            return gameObject.TryGetComponent<T>(out _);
        }

        #endregion

        #region Creating

        public static GameObject Create(this GameObject prefab)
        {
            return Object.Instantiate(prefab);
        }
        
        public static GameObject Create(this GameObject prefab, Vector3 position, Quaternion rotation)
        {
            return Object.Instantiate(prefab, position, rotation);
        }
        
        public static GameObject Create(this GameObject prefab, Transform parent)
        {
            return Object.Instantiate(prefab, parent);
        }
        
        public static GameObject Create(this GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
        {
            GameObject instance = Object.Instantiate(prefab, parent);
            instance.transform.SetPositionAndRotation(position, rotation);
            return instance;
        }
        
        #endregion
    }
}