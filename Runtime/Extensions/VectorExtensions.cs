﻿using UnityEngine;

namespace Opacha.Profit.Extensions.Vectors
{
    public static class VectorExtensions
    {
        #region Presets

        public static readonly Vector3 LeftUp = new Vector3(-1, 1, 0).normalized;
        public static readonly Vector3 LeftDown = new Vector3(-1, -1, 0).normalized;
        public static readonly Vector3 RightUp = new Vector3(1, 1, 0).normalized;
        public static readonly Vector3 RightDown = new Vector3(1, -1, 0).normalized;

        #endregion

        #region Angles and Directions

        public static float AngleTo(this Vector3 start, Vector3 end)
        {
            Vector3 direction = end - start;
            return Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        }
        
        public static Vector3 DirectionXYFrom(float angle)
        {
            float radAngle = Mathf.Deg2Rad * angle;
            return new Vector3(Mathf.Sin(radAngle), Mathf.Cos(radAngle), 0);
        }

        public static Vector3 DirectionTo(this Vector3 start, Vector3 end) => (end - start).normalized;

        #endregion

        #region Setting components

        public static void SetX(this ref Vector3 vector, float x) => vector.x = x;
                        
        public static void SetY(this ref Vector3 vector, float y) => vector.y = y;

        public static void SetZ(this ref Vector3 vector, float z) => vector.z = z;

        public static Vector3 WithX(this Vector3 vector, float x)
        {
            vector.x = x;
            return vector;
        } 
        
        public static Vector3 WithY(this Vector3 vector, float y)
        {
            vector.y = y;
            return vector;
        } 

        public static Vector3 WithZ(this Vector3 vector, float z)
        {
            vector.z = z;
            return vector;
        } 
        
        #endregion
        
        #region Resetting components

        public static void ResetX(this ref Vector3 vector) => vector.x = 0;
                        
        public static void ResetY(this ref Vector3 vector) => vector.y = 0;

        public static void ResetZ(this ref Vector3 vector) => vector.z = 0;
        
        public static Vector3 WithoutX(this Vector3 vector)
        {
            vector.x = 0;
            return vector;
        } 
        
        public static Vector3 WithoutY(this Vector3 vector)
        {
            vector.y = 0;
            return vector;
        } 

        public static Vector3 WithoutZ(this Vector3 vector)
        {
            vector.z = 0;
            return vector;
        } 

        #endregion
    }
}
