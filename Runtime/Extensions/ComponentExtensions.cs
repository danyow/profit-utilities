using UnityEngine;

namespace Opacha.Profit.Extensions.Components
{
    public static class ComponentExtensions
    {
        public static bool TryGetComponentInChildren<T>(this Component source, out T target, bool includeInactive = false)
        {
            target = source.GetComponentInChildren<T>(includeInactive);
            return target != null;
        }

        public static bool TryGetComponentInParent<T>(this Component source, out T target)
        {
            target = source.GetComponentInParent<T>();
            return target != null;
        }
    }
}