﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityRandom = UnityEngine.Random;

namespace Opacha.Profit.Extensions.Randoms
{
    public static class RandomExtensions
    {
        public static T Random<T>(this IList<T> list) => list[UnityRandom.Range(0, list.Count)];

        public static void RandomAction(Action action1, Action action2) => RandomAction(action1, action2, 0.5f);

        public static void RandomAction(Action action1, Action action2, float chance1)
        {
            if (UnityRandom.Range(0f, 1f) < chance1) 
                action1(); 
            else 
                action2();
        }

        public static Quaternion RandomRotationXY()
        {
            float angle = UnityRandom.Range(0, 2 * Mathf.PI);
            return Quaternion.Euler(0, 0, angle * Mathf.Rad2Deg);
        }

        public static Vector2 RandomPosition(float radius) => UnityRandom.insideUnitCircle * radius;
    }
}
