using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Opacha.Profit.Extensions.Collections
{
    public static class CollectionExtensions
    {
        public static void Shuffle<T>(this List<T> list)
        {
            for (int i = 0, count = list.Count; i < count - 1; i++)
            {
                int indexToSwap = Random.Range(i, count);
                T current = list[i];
                list[i] = list[indexToSwap];
                list[indexToSwap] = current;
            }
        }
        
        public static void Shuffle<T>(this T[] array)
        {
            for (int i = 0, count = array.Length; i < count - 1; i++)
            {
                int indexToSwap = Random.Range(i, count);
                T current = array[i];
                array[i] = array[indexToSwap];
                array[indexToSwap] = current;
            }
        }
    }
}