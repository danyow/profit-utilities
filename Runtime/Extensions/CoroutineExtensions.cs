﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Opacha.Profit.Extensions.Coroutines
{
    /* TODO:
      - add caching
      - improve generation of index
      - mapping for each behavior
      - executing all coroutines on separate object ?
    */
    
    public class CoroutinesExecutor : MonoBehaviour { }

    public static class CoroutineHelper
    {
        #region Presets
        
        public static YieldInstruction WaitFrame() => null;
        public static YieldInstruction WaitSeconds(float time) {  return new WaitForSeconds(time); }
        public static YieldInstruction WaitFixedUpdate() {  return new WaitForFixedUpdate(); }
        public static YieldInstruction WaitFrameEnd() {  return new WaitForEndOfFrame(); }
        
        public static CustomYieldInstruction WaitRealSeconds(float time) { return new WaitForSecondsRealtime(time); }
        public static CustomYieldInstruction WaitUntil(Func<bool> condition) { return new WaitUntil(condition); }
        public static CustomYieldInstruction WaitWhile(Func<bool> condition) { return new WaitWhile(condition); }
        
        #endregion

        #region Executor

        private static GameObject executorObject;
        private static MonoBehaviour executorBehaviour;

        public static MonoBehaviour ExecutorBehaviour
        {
            get
            {
                if (!executorObject)
                {
                    executorObject = new GameObject(nameof(CoroutinesExecutor));
                    executorBehaviour = executorObject.AddComponent<CoroutinesExecutor>();
                }
                return executorBehaviour;
            }
        }
        
        #endregion
    }

    public static class CoroutineExtensions
    {
        #region Mapping

        private static Dictionary<int, Coroutine> Coroutines = new Dictionary<int, Coroutine>();

        private static int coroutineIndex = 0;

        private static int GetNextCoroutineIndex() => ++coroutineIndex; 

        private static int MapCoroutine(Func<int, Coroutine> createCoroutine)
        {
            int index = GetNextCoroutineIndex();
            Coroutine coroutine = createCoroutine(index);
            Coroutines[index] = coroutine;
            return index;
        }

        #endregion

        #region Delayed Actions

        private static IEnumerator DelayedRoutine(Action action, YieldInstruction waitInstruction)
        {
            yield return waitInstruction;
            action();
        }
        
        private static IEnumerator DelayedRoutine(Action action, CustomYieldInstruction waitInstruction)
        {
            yield return waitInstruction;
            action();
        }
        
        public static int DelayedAction(this MonoBehaviour behaviour, Action action, float timeDelay)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(DelayedRoutine(
                action + EndRoutineAction(coroutineIndex),
                CoroutineHelper.WaitSeconds(timeDelay))));
        }
        
        public static int DelayedAction(this MonoBehaviour behaviour, Action action, YieldInstruction waitInstruction)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(DelayedRoutine(
                action + EndRoutineAction(coroutineIndex), waitInstruction)));
        }
        
        public static int DelayedAction(this MonoBehaviour behaviour, Action action, CustomYieldInstruction waitInstruction)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(DelayedRoutine(
                action + EndRoutineAction(coroutineIndex), waitInstruction)));
        }
        
        #endregion
        
        #region Repetitive Actions

        private static IEnumerator RepetitiveRoutine(Action action, YieldInstruction waitInstruction)
        {
            while (true)
            {
                action();
                yield return waitInstruction;
            }
        }

        private static IEnumerator RepetitiveRoutine(Action action, CustomYieldInstruction waitInstruction)
        {
            while (true)
            {
                action();
                yield return waitInstruction;
            }
        }
        
        public static int RepetitiveAction(this MonoBehaviour behaviour, Action action, float timeRepetition)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(RepetitiveRoutine(
                action, CoroutineHelper.WaitSeconds(timeRepetition))));
        }
        
        public static int RepetitiveAction(this MonoBehaviour behaviour, Action action, YieldInstruction waitInstruction)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(RepetitiveRoutine(
                action, waitInstruction)));
        }
        
        public static int RepetitiveAction(this MonoBehaviour behaviour, Action action, CustomYieldInstruction waitInstruction)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(RepetitiveRoutine(action, waitInstruction)));
        }
        
        #endregion
        
        #region Interval Actions

        private static IEnumerator IntervalRoutine(Action beforeAction, YieldInstruction waitInstruction, Action afterAction)
        {
            beforeAction();
            yield return waitInstruction;
            afterAction();
        }
        
        private static IEnumerator IntervalRoutine(Action beforeAction, CustomYieldInstruction waitInstruction, Action afterAction)
        {
            beforeAction();
            yield return waitInstruction;
            afterAction();
        }

        public static int IntervalActions(this MonoBehaviour behaviour, Action beforeAction, float timeInterval, Action afterAction)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(IntervalRoutine(
                beforeAction,
                CoroutineHelper.WaitSeconds(timeInterval),
                afterAction + EndRoutineAction(coroutineIndex))));
        }
        
        public static int IntervalActions(this MonoBehaviour behaviour, Action beforeAction, YieldInstruction waitInstruction, Action afterAction)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(IntervalRoutine(
                beforeAction,
                waitInstruction,
                afterAction + EndRoutineAction(coroutineIndex))));
        }
        
        public static int IntervalActions(this MonoBehaviour behaviour, Action beforeAction, CustomYieldInstruction waitInstruction, Action afterAction)
        {
            return MapCoroutine(coroutineIndex => behaviour.StartCoroutine(IntervalRoutine(
                beforeAction,
                waitInstruction,
                afterAction + EndRoutineAction(coroutineIndex))));
        }
        
        #endregion
        
        #region Terminating

        public static void TerminateAction(this MonoBehaviour behaviour, int actionIndex)
        {
            if (Coroutines.ContainsKey(actionIndex))
            {
                behaviour.StopCoroutine(Coroutines[actionIndex]);
                Coroutines.Remove(actionIndex);
            }
        }

        private static Action EndRoutineAction(int index) => () => Coroutines.Remove(index);
        
        public static void _TerminateAllActions(this MonoBehaviour behaviour) => behaviour.StopAllCoroutines();

        public static void _ClearAllActions() => Coroutines.Clear();
        
        #endregion
    }
}
