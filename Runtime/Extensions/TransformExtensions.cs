﻿using Opacha.Profit.Extensions.Vectors;
using UnityEngine;

namespace Opacha.Profit.Extensions.Transforms
{
    public static class TransformExtensions
    {
        #region Hierarchy

        // useful when you need to get a set of homogeneous elements, whose number is changeable
        public static Transform[] GetChildren(this Transform root)
        {
            Transform[] children = new Transform[root.childCount];
            int i = 0;
            
            foreach (Transform child in root)
            {
                children[i++] = child;
            }

            return children;
        }

        public static void DestroyChildren(this Transform root)
        {
            for (int i = root.childCount - 1; i >= 0; i--)
            {
                var child = root.GetChild(i).gameObject;
#if UNITY_EDITOR
                Object.DestroyImmediate(child);
#else
                Object.Destroy(child);
#endif
            }
        }

        #endregion

        #region Angles and Directions

        public static Quaternion AngledRotation(float angle, Vector3 axis)
        {
            return Quaternion.Euler(axis * angle);
        }
        
        public static void SetAngledRotation(this Transform transform, float angle, Vector3 axis)
        {
            transform.rotation = Quaternion.Euler(axis * angle);
        }
        
        public static void SetAngledRotationXY(this Transform transform, float angle)
        {
            transform.rotation = Quaternion.Euler(0, 0,angle);
        }
        
        public static void SetDirectedRotation(this Transform transform, Vector3 end, Vector3 axis)
        {
            float angle = transform.position.AngleTo(end);
            transform.rotation = Quaternion.Euler(axis * angle);
        }
        
        public static void SetDirectedRotationXY(this Transform transform, Vector3 end)
        {
            float angle = transform.position.AngleTo(end);
            transform.rotation = Quaternion.Euler(0, 0, angle);
        }

        public static Vector3 RotationDirection(this Transform transform, Vector3 axis)
        {
            return transform.rotation * axis;
        }
        
        #endregion

        #region Set Position

        public static void SetX(this Transform transform, float x)
        {
            Vector3 position = transform.position;
            position.x = x;
            transform.position = position;
        }
        
        public static void SetY(this Transform transform, float y)
        {
            Vector3 position = transform.position;
            position.y = y;
            transform.position = position;
        }
        
        public static void SetZ(this Transform transform, float z)
        {
            Vector3 position = transform.position;
            position.z = z;
            transform.position = position;
        }

        #endregion
    }
}
