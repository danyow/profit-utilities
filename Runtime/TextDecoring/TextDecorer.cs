using System.Collections.Generic;
using UnityEngine;

namespace Opacha.Profit.TextDecoring
{
    /* TODO:
  - reduce alloc
  - make via delegates?
  - adapt ColorDecorer for color instead of string
*/

    public static class TextDecorer
    {
        private static StyleDecorer styleDecorer = new StyleDecorer();
        private static ColorDecorer colorDecorer = new ColorDecorer();

        public static IDecorer Style(StyleType type)
        {
            styleDecorer.Type = type;
            return styleDecorer;
        }

        public static IDecorer Color(string color)
        {
            colorDecorer.ColorString = color;
            return colorDecorer;
        }

        public static string Style(this string text, StyleType decor)
        {
            styleDecorer.Type = decor;
            return styleDecorer.Decor(text);
        }

        public static string Color(this string text, string color)
        {
            colorDecorer.ColorString = color;
            return colorDecorer.Decor(text);
        }
    }

    public interface IDecorer
    {
        string Decor(string input);
    }

    public class ColorDecorer : IDecorer
    {
        public string ColorString { get; set; }

        private const string colorFormat = "<color=#{0}>{1}</color>";

        public static string FromColor(Color color) => ColorUtility.ToHtmlStringRGB(color);

        public string Decor(string input) => string.Format(colorFormat, ColorString, input);
    }

    public enum StyleType
    {
        Normal,
        Bold,
        Italic,
        ItalicBold,
    }

    public class StyleDecorer : IDecorer
    {
        public StyleType Type { get; set; }

        private Dictionary<StyleType, string> StyleFormats = new Dictionary<StyleType, string>()
        {
            { StyleType.Normal, "{0}" },
            { StyleType.Bold, "<b>{0}</b>" },
            { StyleType.Italic, "<i>{0}</i>" },
            { StyleType.ItalicBold, "<b><i>{0}</i></b>" },
        };
    
        public string Decor(string input) => string.Format(StyleFormats[Type], input);
    }
}